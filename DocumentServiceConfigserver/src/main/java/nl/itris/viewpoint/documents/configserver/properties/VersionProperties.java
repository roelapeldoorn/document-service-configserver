package nl.itris.viewpoint.documents.configserver.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:version.properties")
@ConfigurationProperties(prefix = "nl.itris.ierp.configserver")
public class VersionProperties {
	
	private String version;

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
