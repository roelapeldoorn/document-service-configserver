package nl.itris.viewpoint.documents.configserver;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

import nl.itris.viewpoint.documents.configserver.properties.VersionProperties;

@SpringBootApplication
@EnableConfigServer
public class DocumentServiceConfigserverApplication {

	@Autowired
	private VersionProperties versionProperties;
	
	public static void main(String[] args) {
		SpringApplication.run(DocumentServiceConfigserverApplication.class, args);
	}

    @PostConstruct
    private void postConstruct() {

    	System.out.println("Starting configserver ...");
    	System.out.println("Image version: " + versionProperties.getVersion());
     	
    }
	
}
